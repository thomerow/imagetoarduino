﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using Win32;

namespace ImageToArduino {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		private static readonly Regex RegexInvalidVarNameChars 
			= new Regex(@"[ \.\(\)\+\-\*/\^~=]", RegexOptions.Compiled);

		public MainWindow() {
			InitializeComponent();
		}

		private void BtnDirInpSel_Click(object sender, RoutedEventArgs e) {
			string strFolder = FolderBrowserDialog.SelectFolder("Select a folder", "c:\\", new WindowInteropHelper(this).Handle);
			if (strFolder != null) {
				m_txtDirInp.Text = strFolder;
			}
		}

		private async void BtnGenerate_Click(object sender, RoutedEventArgs e) {
			int count = 0;

			if (string.IsNullOrEmpty(m_txtDirInp.Text)) {
				MessageBox.Show(this, "No folder selected.", "Select a folder", MessageBoxButton.OK, MessageBoxImage.Warning);
				return;
			}

			// Show wait cursor
			IsEnabled = false;
			Mouse.OverrideCursor = Cursors.Wait;

			// Generate header file
			try {
				count = await Generate();
			}
			catch (Exception exc) {
				MessageBox.Show(this, $"Could not generate images. {exc.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}
			finally {
				// Reset cursor
				IsEnabled = true;
				Mouse.OverrideCursor = null;
			}

			if (count == 0) MessageBox.Show(this, "The selected directory does not contain any convertable images.", "No Images", MessageBoxButton.OK, MessageBoxImage.Warning);
			else MessageBox.Show(this, $"File \"images.h\" successfully generated in source folder. Number of added images: {count}", "image.h", MessageBoxButton.OK, MessageBoxImage.Information);
		}

		class ImgInf {
			public byte[] Data { get; set; }
			public Size Size { get; set; }
			public string Name { get; set; }
		}

		private async Task<int> Generate() {
			var sourceImages = EnumerateFilesEx(m_txtDirInp.Text, new string[] { "*.png", "*.bmp", "*.jpg", "*.jpeg", "*.gif" }, SearchOption.TopDirectoryOnly).ToList();
			bool bInvert = (m_chkInvert.IsChecked == true);
			int threshold = (int) m_sldThres.Value;

			ImgInf[] converted = await Task.Run(() => {
				return sourceImages.AsParallel()
					.Select(fileName => new ImgInf {
						Data = Convert(fileName, threshold, bInvert, out Size size),
						Size = size,
						Name = RegexInvalidVarNameChars.Replace(System.IO.Path.GetFileNameWithoutExtension(fileName), "_")
					})
					.Where(inf => (inf.Data != null) && (inf.Data.Length > 0))
					.ToArray();
			});

			if (converted.Length == 0) return 0;

			// Generate header file content:

			// First the needed data structures
			var sb = new StringBuilder();
			sb.AppendLine("struct Size {");
			sb.AppendLine("   const int16_t width;");
			sb.AppendLine("   const int16_t height;");
			sb.AppendLine("};");
			sb.AppendLine();
			sb.AppendLine("struct Image {");
			sb.AppendLine("   const char* name;");
			sb.AppendLine("   const uint8_t* data;");
			sb.AppendLine("   const Size size;");
			sb.AppendLine("};");
			sb.AppendLine();

			// Then the global image data arrays
			string strProgMem = (m_chkStructsProgMem.IsChecked == true) ? "PROGMEM " : "";
			foreach (var imgInf in converted) {
				sb.AppendLine($"const uint8_t Image_{imgInf.Name}_data[{imgInf.Data.Length}] {strProgMem}= {{");
				sb.AppendLine($"   {string.Join(", ", imgInf.Data.Select(b => $"0x{b:X2}"))}");
				sb.AppendLine("};");
				sb.AppendLine();
			}

			// Then the image structs
			sb.AppendLine($"const Image Images[{converted.Length}] {strProgMem}= {{");
			foreach (var imgInf in converted) {
				sb.AppendLine($"   {{ \"{imgInf.Name}\", Image_{imgInf.Name}_data, {{ {imgInf.Size.Width}, {imgInf.Size.Height} }} }},");
			}
			sb.AppendLine("};");
			sb.AppendLine();

			// Write header file to disk
			File.WriteAllText($"{m_txtDirInp.Text}\\images.h", sb.ToString(), Encoding.UTF8);
			return converted.Length;
		}

		/// <summary>
		/// Converts an image to 1 bit black and white data.
		/// </summary>
		/// <param name="fileName">Image file name</param>
		/// <param name="bInvert">Invert colors.</param>
		/// <returns></returns>
		private byte[] Convert(string fileName, int threshold, bool bInvert, out Size size) {
			System.Drawing.Image img = null;
			size = new Size();
			var result = new List<byte>();
			int bitPos = 0;
			byte current = 0;
			byte val;

			try {
				img = System.Drawing.Image.FromFile(fileName);
			}
			catch { return null; }

			if (!(img is System.Drawing.Bitmap bmp)) return null;

			size.Width = bmp.Width;
			size.Height = bmp.Height;

			for (int j = 0; j < bmp.Height; ++j) {
				for (int i = 0; i < bmp.Width; ++i) {
					var col = bmp.GetPixel(i, j);
					int brightness = col.GetPerceivedBrightness();
					val = (brightness > threshold) ? (byte) 1 : (byte) 0;
					if (bInvert) val ^= 1;
					StoreBit(result, ref bitPos, ref current, val);
				}
			}

			// Store last byte
			if (bitPos > 0) result.Add(current);

			return result.ToArray();
		}

		private static void StoreBit(List<byte> result, ref int bitPos, ref byte current, byte val) {
			current |= (byte) ((val << (7 - bitPos++)) & 0xFF);
			if (bitPos == 8) {
				result.Add(current);
				current = 0;
				bitPos = 0;
			}
		}

		public static IEnumerable<string> EnumerateFilesEx(string path, string[] searchPatterns, SearchOption searchOption) {
			return searchPatterns.AsParallel().SelectMany(p => Directory.EnumerateFiles(path, p, searchOption));
		}

	}

	internal static class Extensions {

		public static byte GetPerceivedBrightness(this System.Drawing.Color color) {
			return (byte) Math.Max(0, Math.Min(255, (int) Math.Round(0.299 * color.R + 0.587 * color.G + 0.114 * color.B)));
		}

	}

}
